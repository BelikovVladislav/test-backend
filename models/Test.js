const mongoose = require('mongoose');

const { Schema, SchemaTypes: Types } = mongoose;

const answerScheme = new Schema({
    text: { type: String },
    isTrue: { type: Boolean, default: false },
});

const testSchema = new Schema({
    name: { type: Types.String, required: true },
    questions: [{ type: Types.ObjectId, required: true, ref: 'Question' }],
}, { versionKey: false });


testSchema.statics.publicFields = [
    '_id',
    'name',
    'questions',
];

const test = mongoose.model('test', testSchema);

module.exports = test;
