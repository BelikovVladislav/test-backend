const mongoose = require('mongoose');

const { Schema, SchemaTypes: Types } = mongoose;

const fileSchema = new Schema({
    originalFileName: { type: Types.String, required: true },
    fileName: { type: Types.String, required: false },
}, { versionKey: false });

fileSchema.statics.publicFields = [
    '_id',
    'originalFileName',
    'fileName',
];

const File = mongoose.model('File', fileSchema);

module.exports = File;
