const mongoose = require('mongoose');

const { Schema, SchemaTypes: Types } = mongoose;


const studentStatisticSchema = new Schema({
    test: { type: Types.ObjectId, ref: 'Test' },
    true_questions_count: { type: Types.Number },
    false_questions_count: { type: Types.Number },
    percent: {type: Types.Number },
    passed: {type: Types.Boolean },
    dateTime: {type: Types.Date}
}, { versionKey: false });


studentStatisticSchema.statics.publicFields = [
    '_id',
    'true_questions_count',
    'false_questions_count',
    'percent',
    'passed',
    'dateTime',
];

const studentStatistic = mongoose.model('studentStatistic', studentStatisticSchema);

module.exports = studentStatistic;
