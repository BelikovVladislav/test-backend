const mongoose = require('mongoose');

const { Schema, SchemaTypes: Types } = mongoose;

const categorySchema = new Schema({
    name: { type: Types.String, required: true, unique: true },
    tests: [{ type: Types.ObjectId, required: true, ref: 'Test' }],
}, { versionKey: false });


categorySchema.statics.publicFields = [
    '_id',
    'name',
    'tests',
];

const Category = mongoose.model('category', categorySchema);

module.exports = Category;
