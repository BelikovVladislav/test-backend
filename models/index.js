const fs = require('fs');

/* eslint-disable import/no-dynamic-require */
function getModels() {
    return new Promise((resolve, reject) => {
        fs.readdir(__dirname, (err, files) => {
            if (err) reject(err);

            const models = {};

            files
                .filter(file => !file.includes('index'))
                .forEach((file) => {
                    models[file.split('.')[0]] = require(`./${file}`);
                });

            resolve(models);
        });
    });
}

module.exports = getModels;