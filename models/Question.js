const mongoose = require('mongoose');

const { Schema, SchemaTypes: Types } = mongoose;

const answerScheme = new Schema({
    name: { type: String },
    isTrue: { type: Boolean, default: false },
});

const questionSchema = new Schema({
    name: { type: Types.String, required: true },
    answers: [answerScheme],
    isClosed: {type: Types.Boolean, required: true}
}, { versionKey: false });


questionSchema.statics.publicFields = [
    '_id',
    'name',
    'answers',
];

const question = mongoose.model('question', questionSchema);

module.exports = question;
