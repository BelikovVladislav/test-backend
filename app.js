const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('./utitlites/mongoose');
const fileUpload = require('express-fileupload');
const indexRouter = require('./routes/index');
const { bindModels } = require('./utitlites/middleware');

const app = express();

app.use(logger('dev'));
app.use(bindModels);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/file', express.static(__dirname + 'files'));
app.use(fileUpload({useTempFiles: true, tempFileDir: path.join(__dirname, 'public/uploads')}));
global.__basedir = __dirname;
app.use('/', indexRouter);

module.exports = app;
