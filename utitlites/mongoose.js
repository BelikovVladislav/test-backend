const mongoose = require('mongoose');
const db = require('../config/db');
console.log(db);
mongoose.Promise = Promise;

const mongoUri = `mongodb://${db.host}:${db.port}/${db.name}`;


mongoose.connection.openUri(mongoUri);

module.exports = mongoose;