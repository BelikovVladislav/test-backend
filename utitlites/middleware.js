const getModels = require('../models');

async function bindModels(req, res, next) {
    req.models = await getModels();
    await next();
}

module.exports  = { bindModels };