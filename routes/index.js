var express = require('express');
var router = express.Router();
const path = require('path');
const fs = require('fs-promise');
const moment = require('moment');

const uploadFile = async (file, dir, fileName) => {
    if (!fs.existsSync(dir)) {
        await fs.mkdir(dir);
    }

    const filePath = path.join(dir, fileName);
    const reader = fs.createReadStream(file.tempFilePath);
    const writer = fs.createWriteStream(filePath);
    reader.pipe(writer);
    console.log(filePath);
};

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/check', (req, res) => {
    res.send("ok");
});

router.get('/categories', async (req, res) => {
    const { Category } = req.models;
    let categories = await Category.find();
    categories = categories.map(c => ({ ...c.toObject(), tests: [] }));
    categories.sort((a, b) => a.name.localeCompare(b.name));
    res.send(categories);
});

router.get('/tests/:categoryID', async (req, res) => {
    const { Category, Test } = req.models;
    const category = await Category.findOne({ _id: req.params.categoryID });
    const { tests = [] } = category;
    const resultTests = tests && tests.length ? await Test.find({ _id: { $in: tests } }) : tests;
    if (resultTests) {
        resultTests.sort((a, b) => a.name.localeCompare(b.name));
        res.send(resultTests.map(t => ({ ...t.toObject(), questions: [] })));
    } else {
        res.send(null);
    }
});

router.get('/check-category-exists/:name', async (req, res) => {
    const { Category } = req.models;
    const category = await Category.findOne({ name: req.params.name });
    res.send(!!category);
});

router.post('/category', async (req, res) => {
    const { Category } = req.models;
    const category = await Category.create({ ...req.body });
    res.send(category);
});

router.post('/test/:categoryID', async (req, res) => {
    const { Category, Test, Question } = req.models;
    const category = await Category.findOne({ _id: req.params.categoryID });
    const rawTest = req.body;
    const questions = await Promise.all(rawTest.questions.map(q => {
        delete q._id;
        return Question.create(q)
    }));
    delete rawTest._id;
    const test = await Test.create({ ...rawTest, questions: questions.map((q) => q._id) });
    console.log(category);
    category.tests = category.tests || [];
    category.tests.push(test._id);
    await category.save();
    res.send(test._id);
});

router.get('/test/:testID', async (req, res) => {
    const { Test, Question } = req.models;
    const rawTest = await Test.findOne({ _id: req.params.testID });
    const questions = await Question.find({ _id: { $in: rawTest.questions } });
    const result = {
        ...rawTest.toObject(),
        questions: questions.map(q => q.toObject())
    };
    res.send(result);
});


router.get('/files', async (req, res) => {
    const { File } = req.models;
    const files = await File.find();
    res.send(files);
});

router.post('/file', async (req, res) => {
    const File = req.models.File;
    const filePath = path.join(
        __basedir
        , 'files');
    const createdFiles = [];
    path.root

    const files = Object.values(req.files || {});

    /* eslint-disable no-await-in-loop */
    for (const file of files) {
        const dbFile = await File.create({
            fileName: '',
            originalFileName: file.name
        });

        const generatedFileName = dbFile._id + file.name.substr(file.name.lastIndexOf('.'));

        await uploadFile(file, filePath, generatedFileName);

        await File.update({ _id: dbFile._id }, { $set: { fileName: generatedFileName } });
        dbFile.fileName = generatedFileName;

        createdFiles.push(dbFile);
    }
    res.send(createdFiles.pop());
});

router.get('/file/:name', async (req, res) => {
    const { File } = req.models;
    const file = await File.findOne({ fileName: req.params.name });
    const path = `${__basedir}/files/${file.fileName}`;
    res.download(path, file.originalFileName);
});

router.delete('/file/:id', async (req, res) => {
    const { File } = req.models;
    const _id = req.params.id;
    const file = await File.findOne({ _id });
    if (file) {
        const filePath = path.join(__basedir, `/files/${ file.fileName }`);
        fs.unlink(filePath).catch(() => {});
        await File.deleteOne({ _id })
    }
    res.sendStatus(200);

});

router.post('/statistic', async (req, res) => {
    const { StudentStatistic } = req.models;
    const result = await StudentStatistic.create({
        ...req.body,
        test: req.body.test._id,
    });
    res.sendStatus(200);
});

router.get('/statistic', async (req, res) => {
    const { StudentStatistic } = req.models;
    const period = req.query.period || 'За 7 дней';
    const options = {};
    switch (period) {
        case 'За 7 дней':
            options.dateTime = { $gte: moment().subtract('days', 7).toISOString() };
            break;
        case 'За месяц':
            options.dateTime = { $gte: moment().subtract('months', 1).toISOString() };
            break;
        default:
            break;
    }
    const result = {
        period,
        testCount: 0,
        passedCount: 0,
        percent: 0,
    };
    const statistic = await StudentStatistic.find(options);
    statistic.forEach(element => {
        result.testCount++;
        if (element.passed) {
            result.passedCount++;
        }
    });
    if (result.testCount) {
        result.percent = (result.passedCount / result.testCount) * 100
    }

    res.send(result);
});

module.exports = router;
